﻿unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, System.hash,  Vcl.Buttons, Winapi.ShellAPI,
  Crypt2,
  System.NetEncoding,
  System.DateUtils,
  DECHash, DECFormat,
  superobject,
  CnSHA3,CnCRC32,
  HlpHashFactory, HlpConverters, HlpIHashInfo,
  flcCryptoUtils, flcCryptoHash, flcHash;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Memo1: TMemo;
    Button2: TButton;
    Label3: TLabel;
    Edit3: TEdit;
    OpenDialog1: TOpenDialog;
    SpeedButton1: TSpeedButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    procedure TFfile(var Msg:TMessage);message WM_DROPFILES;

  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation


{$R *.dfm}

function DateTimeToUnix(const AValue: TDateTime): Int64;
// 日期转Unix时间戳
begin
  Result := System.DateUtils.DateTimeToUnix(AValue) - 8 * 60 * 60;
end;

function GetStrHashMD5(Str: string): string;
var
  HashMD5: THashMD5;
begin
  HashMD5 := THashMD5.Create;
  HashMD5.GetHashString(Str);
  Result := HashMD5.GetHashString(Str);
end;

function GetStrHashSHA1(Str: string): string;
var
  HashSHA: THashSHA1;
begin
  HashSHA := THashSHA1.Create;
  HashSHA.GetHashString(Str);
  Result := HashSHA.GetHashString(Str);
end;

// SHA2 的通用方法函数
function GetStrHashSHA2(Str: string; HashVersion : THashSHA2.TSHA2Version = SHA256): string;
var
  HashSHA: THashSHA2;
begin
  HashSHA := THashSHA2.Create;
  HashSHA.GetHashString(Str);
  Result := HashSHA.GetHashString(Str, HashVersion);
end;

function GetStrHashBobJenkins(Str: string): string;
var
  hash: THashBobJenkins;
begin
  hash := THashBobJenkins.Create;
  hash.GetHashString(Str);
  Result := hash.GetHashString(Str);
end;

// Examples calculating file hashes:
function GetFileHashMD5(FileName: WideString): string;
var
  HashMD5: THashMD5;
  Stream: TStream;
  Readed: Integer;
  Buffer: PByte;
  BufLen: Integer;
begin
  HashMD5 := THashMD5.Create;
  BufLen := 16 * 1024;
  Buffer := AllocMem(BufLen);
  try
    Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
      while Stream.Position < Stream.Size do
      begin
        Readed := Stream.Read(Buffer^, BufLen);
        if Readed > 0 then
        begin
          HashMD5.update(Buffer^, Readed);
        end;
      end;
    finally
      Stream.Free;
    end;
  finally
    FreeMem(Buffer)
  end;
  Result := HashMD5.HashAsString;
end;

function GetFileHashSHA1(FileName: WideString): string;
var
  HashSHA: THashSHA1;
  Stream: TStream;
  Readed: Integer;
  Buffer: PByte;
  BufLen: Integer;
begin
  HashSHA := THashSHA1.Create;
  BufLen := 16 * 1024;
  Buffer := AllocMem(BufLen);
  try
    Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
      while Stream.Position < Stream.Size do
      begin
        Readed := Stream.Read(Buffer^, BufLen);
        if Readed > 0 then
        begin
          HashSHA.update(Buffer^, Readed);
        end;
      end;
    finally
      Stream.Free;
    end;
  finally
    FreeMem(Buffer)
  end;

  Result := HashSHA.HashAsString;

end;

function GetFileHashSHA2(FileName: WideString; HashVersion : THashSHA2.TSHA2Version = SHA256): string;
var
  HashSHA: THashSHA2;
  Stream: TStream;
  Readed: Integer;
  Buffer: PByte;
  BufLen: Integer;
begin
  HashSHA := THashSHA2.Create(HashVersion);
  BufLen := 16 * 1024;
  Buffer := AllocMem(BufLen);
  try
    Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
      while Stream.Position < Stream.Size do
      begin
        Readed := Stream.Read(Buffer^, BufLen);
        if Readed > 0 then
        begin
          HashSHA.update(Buffer^, Readed);
        end;
      end;
    finally
      Stream.Free;
    end;
  finally
    FreeMem(Buffer)
  end;
  Result := HashSHA.HashAsString;
end;

function GetFileHashBobJenkins(FileName: WideString): string;
var
  hash: THashBobJenkins;
  Stream: TStream;
  Readed: Integer;
  Buffer: PByte;
  BufLen: Integer;
begin
  hash := THashBobJenkins.Create;
  BufLen := 16 * 1024;
  Buffer := AllocMem(BufLen);
  try
    Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
      while Stream.Position < Stream.Size do
      begin
        Readed := Stream.Read(Buffer^, BufLen);
        if Readed > 0 then
        begin
          hash.update(Buffer^, Readed);
        end;
      end;
    finally
      Stream.Free;
    end;
  finally
    FreeMem(Buffer)
  end;
  Result := hash.HashAsString;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  rsl: string;
  skey: Tbytes;
  CRC: DWORD;
begin
  Memo1.Clear;

  rsl := GetStrHashMD5(Edit1.Text);
  Memo1.Lines.Add('MD5:' + rsl);

  rsl := GetStrHashSHA1(Edit1.Text);
  Memo1.Lines.Add(#13 + #10 + 'SHA1:' + rsl);

  rsl := GetStrHashSHA2(Edit1.Text, SHA224);
  Memo1.Lines.Add(#13 + #10 + 'SHA224:' + rsl);

  rsl := GetStrHashSHA2(Edit1.Text, SHA256);
  Memo1.Lines.Add(#13 + #10 + 'SHA256:' + rsl);

  rsl := GetStrHashSHA2(Edit1.Text, SHA384);
  Memo1.Lines.Add(#13 + #10 + 'SHA384:' + rsl);

  rsl := GetStrHashSHA2(Edit1.Text, SHA512);
  Memo1.Lines.Add(#13 + #10 + 'SHA512:' + rsl);

  rsl := GetStrHashSHA2(Edit1.Text, SHA512_224);
  Memo1.Lines.Add(#13 + #10 + 'SHA512_224:' + rsl);

  rsl := GetStrHashSHA2(Edit1.Text, SHA512_256);
  Memo1.Lines.Add(#13 + #10 + 'SHA512_256:' + rsl);

  rsl := GetStrHashBobJenkins(Edit1.Text);
  Memo1.Lines.Add(#13 + #10 + 'BobJenkins:' + rsl);

  rsl := THashSHA2.GetHMAC(Edit1.Text, Edit2.Text, SHA224);
  Memo1.Lines.Add(#13 + #10 + 'HMAC_SHA224:' + rsl);

  rsl := THashSHA2.GetHMAC(Edit1.Text, Edit2.Text);
  Memo1.Lines.Add(#13 + #10 + 'HMAC_SHA256:' + rsl);

  rsl := THashSHA2.GetHMAC(Edit1.Text, Edit2.Text, SHA384);
  Memo1.Lines.Add(#13 + #10 + 'HMAC_SHA384:' + rsl);

  rsl := THashSHA2.GetHMAC(Edit1.Text, Edit2.Text, SHA512);
  Memo1.Lines.Add(#13 + #10 + 'HMAC_SHA512:' + rsl);

  rsl := THashSHA2.GetHMAC(Edit1.Text, Edit2.Text, SHA512_224);
  Memo1.Lines.Add(#13 + #10 + 'HMAC_SHA512_224:' + rsl);

  rsl := THashSHA2.GetHMAC(Edit1.Text, Edit2.Text, SHA512_256);
  Memo1.Lines.Add(#13 + #10 + 'HMAC_SHA512_256:' + rsl);

  // 二进制转 Base64 编码输出结果
  skey := THashSHA2.GetHMACAsBytes(Edit1.Text,
    Tencoding.UTF8.GetBytes(Edit2.Text), SHA256);
  rsl := TBase64Encoding.Base64.EncodeBytesToString(skey);
  Memo1.Lines.Add(#13 + #10 + 'HMAC_SHA256_AsBytes Base64:' +#13#10+ rsl);
  Memo1.Lines.Add('Checked With Online: https://www.devglan.com/online-tools/hmac-sha256-online');


  rsl := THashMD5.GetHMAC(Edit1.Text, Edit2.Text);
  Memo1.Lines.Add(#13 + #10 + 'HMAC_MD5:' + rsl);

  rsl := THashSHA1.GetHMAC(Edit1.Text, Edit2.Text);
  Memo1.Lines.Add(#13 + #10 + 'HMAC_SHA1:' + rsl);


  // 文件Hash
  if Trim(Edit3.Text) = '' then
  begin
    Exit;
  end;
  Memo1.Lines.Add('');
  Memo1.Lines.Add('文件Hash结果：');
  rsl := GetFileHashMD5(Edit3.Text);
  Memo1.Lines.Add(#13 + #10 + 'FileHashMD5:' + rsl);

  rsl := GetFileHashSHA1(Edit3.Text);
  Memo1.Lines.Add(#13 + #10 + 'FileHashSHA1:' + rsl);

  rsl := GetFileHashSHA2(Edit3.Text, SHA224);
  Memo1.Lines.Add(#13 + #10 + 'FileHashSHA224:' + rsl);

  rsl := GetFileHashSHA2(Edit3.Text, SHA256);
  Memo1.Lines.Add(#13 + #10 + 'FileHashSHA256:' + rsl);

  rsl := GetFileHashSHA2(Edit3.Text, SHA384);
  Memo1.Lines.Add(#13 + #10 + 'FileHashSHA384:' + rsl);

  rsl := GetFileHashSHA2(Edit3.Text, SHA512);
  Memo1.Lines.Add(#13 + #10 + 'FileHashSHA512:' + rsl);

  rsl := GetFileHashSHA2(Edit3.Text, SHA512_224);
  Memo1.Lines.Add(#13 + #10 + 'FileHashSHA512_224:' + rsl);

  rsl := GetFileHashSHA2(Edit3.Text, SHA512_256);
  Memo1.Lines.Add(#13 + #10 + 'FileHashSHA512_256:' + rsl);

  rsl := GetFileHashBobJenkins(Edit3.Text);
  Memo1.Lines.Add(#13 + #10 + 'FileHashBobJenkins:' + rsl);

  rsl := SHA3_256Print(SHA3_256File(Edit3.Text));
  Memo1.Lines.Add(#13 + #10 + 'SHA3_256Hash_wich_cnvcl:' + rsl);

  // cnpack 检查CRC32值
  CRC := 0;
  if CnCRC32.FileCRC32(Edit3.Text, CRC, 0, 0) then
  begin
    Memo1.Lines.Add(#13 + #10 + 'CnCRC32计算CRC32：' + LowerCase(IntToHex(CRC, 2)));
  end;

end;

procedure TForm1.Button2Click(Sender: TObject);
var
  crypt: HCkCrypt2;
  sData: PWideChar;
  msg: PWideChar;
begin

{

  https://www.chilkatsoft.com/refdoc/dd_CkCrypt2Ref.html
  The valid choices are "sha1", "sha256", "sha384", "sha512", "sha3-224", "sha3-256", "sha3-384", "sha3-512", "md2", "md5", "haval", "ripemd128", "ripemd160","ripemd256", or "ripemd320".

  Note: SHA-2 designates a set of cryptographic hash functions that includes SHA-256, SHA-384, and SHA-512. Chilkat by definition supports "SHA-2" because it supports these algorithms.

}
  // This example assumes Chilkat Crypt2 to have been previously unlocked.
  // See Unlock Crypt2 for sample code.
  // Demonstrates how to reproduce the following results found at
  // https://en.wikipedia.org/wiki/Hash-based_message_authentication_code
  // HMAC_SHA256("key", "The quick brown fox jumps over the lazy dog") = 0xf7bc83f430538424b13298e6aa6fb143ef4d59a14946175997479dbc2d1a3cd8
  // HMAC_MD5("key", "The quick brown fox jumps over the lazy dog")    = 0x80070713463e7749b90c2dc24911e275
  // HMAC_SHA1("key", "The quick brown fox jumps over the lazy dog")   = 0xde7c9b85b8b78aa6bc8a7a36f70a90701c9db4d9

  crypt := CkCrypt2_Create();

  //  设置字符集
  CkCrypt2_putCharset(crypt,'utf-8');  //!!!!!!关键语句，否则无法正常编码中文

  CkCrypt2_putMacAlgorithm(crypt, 'hmac');
  CkCrypt2_putEncodingMode(crypt, 'hexlower');
  // set Key
  CkCrypt2_SetMacKeyString(crypt, PWideChar((Edit2.Text)));

  // get Data to HMAC
  sData := PWideChar(string(Edit1.Text));

  CkCrypt2_putHashAlgorithm(crypt, 'SHA-256');
  Memo1.Lines.Add('HMAC_SHA256: ' + CkCrypt2__macStringENC(crypt, sData));

  CkCrypt2_putHashAlgorithm(crypt, 'MD5');
  Memo1.Lines.Add('HMAC_MD5: ' + CkCrypt2__macStringENC(crypt, sData));

  CkCrypt2_putHashAlgorithm(crypt, 'SHA-1');
  Memo1.Lines.Add('HMAC_SHA1: ' + CkCrypt2__macStringENC(crypt, sData));

  // The output is the same (ignore case)
  // HMAC_SHA256: F7BC83F430538424B13298E6AA6FB143EF4D59A14946175997479DBC2D1A3CD8
  // HMAC_MD5: 80070713463E7749B90C2DC24911E275
  // HMAC_SHA1: DE7C9B85B8B78AA6BC8A7A36F70A90701C9DB4D9

  CkCrypt2_Dispose(crypt);

  // This example requires Chilkat v9.5.0.83 or greater.
  crypt := CkCrypt2_Create();

  // 设置输出编码格式
  CkCrypt2_putEncodingMode(crypt,'hexlower');
  { hexlower 小写输出
   hex 大写
   "modBase64",
   "base64url",
    "Base32",
    "Base58",
    "UU", "QP" (for quoted-printable),
    "URL" (for url-encoding),
    "Hex", "Q", "B",
    "url_oauth", "url_rfc1738", "url_rfc2396", "url_rfc3986",
    "fingerprint", or "decimal".
  }

  //  设置字符集
  CkCrypt2_putCharset(crypt,'utf-8');  //!!!!!!关键语句，否则无法正常编码中文

  msg := PChar(Edit1.Text);

  // 设定 hash 模式
  CkCrypt2_putHashAlgorithm(crypt,'sha3-224');

  Memo1.Lines.Add('sha3-224:        ' + CkCrypt2__hashStringENC(crypt,msg));
  // Or hash a file..
  // hash2 := CkCrypt2__hashFileENC(crypt,'qa_data/hamlet.xml');

  CkCrypt2_putHashAlgorithm(crypt,'sha256');
  Memo1.Lines.Add('sha256:        ' + CkCrypt2__hashStringENC(crypt,msg));
  {
    putHashAlgorithm：
    "sha1",
    "sha256", "sha384", "sha512",
    "sha3-224", "sha3-256", "sha3-384", "sha3-512",
    "md2", "md5", "haval",
    "ripemd128", "ripemd160","ripemd256", or "ripemd320".
  }

  CkCrypt2_Dispose(crypt);

end;

// 腾讯云TC3 V3签名鉴权
procedure TForm1.Button3Click(Sender: TObject);
var
  host: string;
  httpRequestMethod: string; // = "POST";
  canonicalUri: string; // = "/";
  canonicalQueryString: string; // = "";
  canonicalHeaders: string;
  // = "content-type:application/json; charset=utf-8\n" + "host:" + host + "\n";
  signedHeaders: string; // = "content-type;host";
  SecretKey, Service: string;
  SecretDate, SecretService, SecretSigning, Signature: Tbytes;
  StringToSign, payload, CanonicalRequest, HashedRequestPayload,
    HashedCanonicalRequest: string;
  sDate, timestamp: string;
  Authorization, SecretId, CredentialScope: string;
begin
  sDate := FormatDateTime('YYYY-MM-DD', now());
  timestamp := DateTimeToUnix(now).ToString;
  host := 'ocr.tencentcloudapi.com';
  httpRequestMethod := 'POST';
  canonicalUri := '/';
  canonicalQueryString := '';
  canonicalHeaders := 'content-type:application/json' + #10 + 'host:' +
    host + #10;
  signedHeaders := 'content-type;host';

  payload :=
    '{"ImageUrl":"http://sub.gxnews.com.cn/subject/2021/2021cj/img/image_4.jpg"}';
  // 待发送的数据的哈希值:
  HashedRequestPayload := THashSHA2.GetHashString(payload);

  // 拼接规范请求串
  CanonicalRequest := httpRequestMethod + #10 + canonicalUri + #10 +
    canonicalQueryString + #10 + canonicalHeaders + #10 + signedHeaders + #10 +
    HashedRequestPayload;

  // 计算派生签名密钥
  SecretKey := '2rWsFMw';
  SecretDate := THashSHA2.GetHMACAsBytes(sDate,
    Tencoding.UTF8.GetBytes('TC3' + SecretKey));
  Service := 'ocr';
  SecretService := THashSHA2.GetHMACAsBytes(Service, SecretDate);
  SecretSigning := THashSHA2.GetHMACAsBytes('tc3_request', SecretService);

  // 规范请求串的哈希值
  HashedCanonicalRequest := THashSHA2.GetHashString(CanonicalRequest);
  // 组装待签名字符串
  StringToSign := 'TC3-HMAC-SHA256' + #10 + timestamp + #10 + sDate +
    '/ocr/tc3_request' + #10 + HashedCanonicalRequest;

  // 计算签名
  Signature := THashSHA2.GetHMACAsBytes(Bytesof(StringToSign), SecretSigning);

  // Application.MessageBox(PChar(THash.DigestAsString(Signature)),
  // '提示', MB_OK + MB_ICONINFORMATION + MB_TOPMOST);

  SecretId := 'AKIDFm4wMEAH';
  CredentialScope := sDate + '/ocr/tc3_request';
  // 拼接 Authorization
  Authorization := 'TC3-HMAC-SHA256' + ' ' + 'Credential=' + SecretId + '/' +
    CredentialScope + ', ' + 'SignedHeaders=' + signedHeaders + ', ' +
    'Signature=' + PChar(THash.DigestAsString(Signature));

  Memo1.Lines.Add(Authorization);
end;

procedure TForm1.Button4Click(Sender: TObject); //DECHash
var
  Res: Tbytes;
  hash: THash_SHA256;
  hashMd5 : THash_MD5;
  dstr: string;
  ww : RawByteString;
begin
{
  hash := THash_SHA256.Create;
  try
    hash.Init;
    Res := hash.HMAC(Tencoding.UTF8.GetBytes(Edit2.Text),
      Tencoding.UTF8.GetBytes(Edit1.Text));

    // dstr := THash.DigestAsString(res);
    dstr := LowerCase(StringOf(ValidFormat(TFormat_HEX).Encode(Res)));
  finally
    hash.Free;
  end;
  Memo1.Lines.Add('create 方法:' + #13#10 + dstr);
}
  Memo1.Clear;
  hash := THash_SHA256.Create;
  ww := hash.CalcString(Utf8Encode(Edit1.Text), TFormat_HEXL);
  Setstring(dstr, PAnsiChar(ww), Length(ww));
  Memo1.Lines.Add('-----待加密字符串的 SHA256:-----' + #13#10 + dstr);
  Memo1.Lines.Add('');
  hash.Free;

  // 使用 Utf8Encode() 转码很重要！！
  hashMd5 := THash_MD5.Create;
  ww := hashMd5.CalcString(Utf8Encode(Edit1.Text), TFormat_HEXL);
  Setstring(dstr, PAnsiChar(ww), Length(ww));
  Memo1.Lines.Add('-----待加密字符串的MD5:-----' + #13#10 + dstr);
  Memo1.Lines.Add('');
  hashMd5.Free;



  Res := THash_MD2.HMAC(Tencoding.UTF8.GetBytes(Edit2.Text),
    Tencoding.UTF8.GetBytes(Edit1.Text));
  Memo1.Lines.Add('-----HMAC_MD2:-----' + #13#10 + PChar(THash.DigestAsString(Res)));
  Memo1.Lines.Add('');
  Res := THash_MD4.HMAC(Tencoding.UTF8.GetBytes(Edit2.Text),
    Tencoding.UTF8.GetBytes(Edit1.Text));
  Memo1.Lines.Add('-----HMAC_MD4:-----' + #13#10 + PChar(THash.DigestAsString(Res)));
  Memo1.Lines.Add('');
  Res := THash_MD5.HMAC(Tencoding.UTF8.GetBytes(Edit2.Text),
    Tencoding.UTF8.GetBytes(Edit1.Text));
  Memo1.Lines.Add('-----HMAC_MD5:-----' + #13#10 + PChar(THash.DigestAsString(Res)));
  Memo1.Lines.Add('');
  Res := THash_SHA0.HMAC(Tencoding.UTF8.GetBytes(Edit2.Text),
    Tencoding.UTF8.GetBytes(Edit1.Text));
  Memo1.Lines.Add('-----HMAC_SHA0:-----' + #13#10 + PChar(THash.DigestAsString(Res)));
  Memo1.Lines.Add('');
  Res := THash_SHA1.HMAC(Tencoding.UTF8.GetBytes(Edit2.Text),
    Tencoding.UTF8.GetBytes(Edit1.Text));
  Memo1.Lines.Add('-----HMAC_SHA1:-----' + #13#10 + PChar(THash.DigestAsString(Res)));
  Memo1.Lines.Add('');
  Res := THash_Tiger.HMAC(Tencoding.UTF8.GetBytes(Edit2.Text),
    Tencoding.UTF8.GetBytes(Edit1.Text));
  Memo1.Lines.Add('-----HMAC_Tiger:-----' + #13#10 + PChar(THash.DigestAsString(Res)));
  Memo1.Lines.Add('');

  Res := THash_SHA256.HMAC(Tencoding.UTF8.GetBytes(Edit2.Text),
         Tencoding.UTF8.GetBytes(Edit1.Text));
  Memo1.Lines.Add('-----HMAC_SHA256:-----' + #13#10 + PChar(THash.DigestAsString(Res)));
  dstr := LowerCase(StringOf(TFormat_Base64.Encode(Res)));
  Memo1.Lines.Add('-----HMAC_SHA256 with Base65 Coding:' + #13#10 + dstr);
  Memo1.Lines.Add('');


  Res := THash_SHA384.HMAC(Tencoding.UTF8.GetBytes(Edit2.Text),
    Tencoding.UTF8.GetBytes(Edit1.Text));
  Memo1.Lines.Add('-----HMAC_SHA384:-----' + #13#10 + PChar(THash.DigestAsString(Res)));
  Memo1.Lines.Add('');

  Res := THash_SHA512.HMAC(Tencoding.UTF8.GetBytes(Edit2.Text),
    Tencoding.UTF8.GetBytes(Edit1.Text));
  Memo1.Lines.Add('-----HMAC_SHA512:-----' + #13#10 + PChar(THash.DigestAsString(Res)));
  Memo1.Lines.Add('');

  Res := THash_RipeMD256.HMAC(Tencoding.UTF8.GetBytes(Edit2.Text),
    Tencoding.UTF8.GetBytes(Edit1.Text));
  Memo1.Lines.Add('-----HMAC_RipeMD256:-----' + #13#10 + PChar(THash.DigestAsString(Res)));
  Memo1.Lines.Add('');

  Res := THash_Haval256.HMAC(Tencoding.UTF8.GetBytes(Edit2.Text),
    Tencoding.UTF8.GetBytes(Edit1.Text));
  Memo1.Lines.Add('-----HMAC_Haval56:-----' + #13#10 + PChar(THash.DigestAsString(Res)));
  Memo1.Lines.Add('');

  //文件Hash
  if Edit3.Text = '' then exit;
  hash :=  THash_SHA256.Create;
  try
    ww := hash.CalcFile(Edit3.Text, TFormat_HEXL);
    Setstring(dstr, PAnsiChar(ww), Length(ww));
    Memo1.Lines.Add('Get_File_SHA256: ' + dstr);
  finally
    hash.Free;
  end;

end;

procedure TForm1.Button5Click(Sender: TObject);
var
  sjson, val: string;
  obj: ISuperObject;
  aSuperArray: TSuperArray;
  i: Integer;
begin
  sjson := '{"error":0,"status":"success","date":"2014-03-04","results":[{"currentCity":"成都",'
    + '"weather_data":[{"date":"周二(今天, 实时：12℃)","dayPictureUrl":' +
    '"http://api.map.baidu.com/images/weather/day/duoyun.png","nightPictureUrl":'
    + '"http://api.map.baidu.com/images/weather/night/duoyun.png","weather":"多云",'
    + '"wind":"北风微风","temperature":"15 ~ 6℃"},{"date":"周三","dayPictureUrl":' +
    '"http://api.map.baidu.com/images/weather/day/yin.png","nightPictureUrl":' +
    '"http://api.map.baidu.com/images/weather/night/xiaoyu.png","weather":"阴转小雨",'
    + '"wind":"北风微风","temperature":"14 ~ 7℃"},{"date":"周四","dayPictureUrl":' +
    '"http://api.map.baidu.com/images/weather/day/xiaoyu.png","nightPictureUrl":'
    + '"http://api.map.baidu.com/images/weather/night/xiaoyu.png","weather":"小雨",'
    + '"wind":"北风微风","temperature":"12 ~ 7℃"},{"date":"周五","dayPictureUrl":' +
    '"http://api.map.baidu.com/images/weather/day/xiaoyu.png","nightPictureUrl":'
    + '"http://api.map.baidu.com/images/weather/night/xiaoyu.png","weather":"小雨",'
    + '"wind":"南风微风","temperature":"9 ~ 6℃"}]}]}';

  obj := SO(sjson);
  val := obj['results'].AsString; // get a string
  aSuperArray := obj['results'].AsArray;
  for i := 0 to aSuperArray.Length - 1 do
    Memo1.Lines.Add(aSuperArray[i]['currentCity'].AsString + aSuperArray[i]
      ['weather_data'].AsString);
  Memo1.Lines.Add('');
  aSuperArray := aSuperArray[0]['weather_data'].AsArray;
  Memo1.Lines.Add(aSuperArray[3].S['temperature']);
end;

procedure TForm1.Button6Click(Sender: TObject);
var
  Password, Salt: TBytes;
  rsl : string;
  LHMAC: IHMAC;
begin
  Password := TConverters.ConvertStringToBytes('password', TEncoding.UTF8);
  Salt := TConverters.ConvertStringToBytes('salt', TEncoding.UTF8);
  rsl := THashFactory.TCrypto.CreateSHA2_256.ComputeString(edit1.Text, TEncoding.UTF8).ToString();
  Memo1.Lines.Add('SHA2_256:' + rsl);
  Memo1.Lines.Add('');

  LHMAC := THashFactory.THMAC.CreateHMAC(THashFactory.TCrypto.CreateMD5);
  LHMAC.Key := TConverters.ConvertStringToBytes(Edit2.Text, TEncoding.UTF8);
  rsl := LHMAC.ComputeString(Edit1.Text, TEncoding.UTF8).ToString();

  Memo1.Lines.Add('HMAC_MD5:' + rsl);
//   TKDF.TPBKDF2_HMAC.CreatePBKDF2_HMAC
//   (THashFactory.TCrypto.CreateSHA2_256(), Password, Salt, 100000);
  rsl := '{SHA}' +
    THashFactory.TCrypto.CreateSHA1().ComputeString(
    'myPassword', TEncoding.UTF8).ToString;
  Memo1.Lines.Add('SHA:' + rsl);
  Memo1.Lines.Add('');

  LHMAC := THashFactory.THMAC.CreateHMAC(THashFactory.TCrypto.CreateSHA2_256,
      TConverters.ConvertStringToBytes(Edit2.Text, TEncoding.UTF8));
  rsl  := LHMAC.ComputeString(Edit1.Text, TEncoding.UTF8).ToString();
  Memo1.Lines.Add('HMAC_SHA256_in_one:' + rsl);

  Memo1.Lines.Add('');
  LHMAC := THashFactory.THMAC.CreateHMAC(THashFactory.TCrypto.CreateSHA3_512,
      TConverters.ConvertStringToBytes(Edit2.Text, TEncoding.UTF8));
  rsl  := LHMAC.ComputeString(Edit1.Text, TEncoding.UTF8).ToString();
  Memo1.Lines.Add('HMAC_SHA3_512_ALL_in_one:' + rsl);

  Memo1.Lines.Add('');
  rsl := THashFactory.TCrypto.CreateSHA3_512.ComputeString(Edit1.Text, TEncoding.UTF8).ToString;
  Memo1.Lines.Add('SHA3_512_in_one:' + rsl);

  // Hash File
  if Edit3.Text <> '' then
  begin
    rsl := THashFactory.TCrypto.CreateSHA2_256.ComputeFile(Edit3.Text).ToString();
    Memo1.Lines.Add('File Hash SHA2:' + rsl);
  end;

end;

procedure TForm1.Button7Click(Sender: TObject); //fundamentals5
  function DupChar(const Count: Integer; const Ch: Byte): RawByteString;
    var
      A : RawByteString;
      I : Integer;
    begin
      A := '';
      for I := 1 to Count do
        A := A + AnsiChar(Ch);
      Result := A;
    end;
//fundamentals5-master flcCryptoHash.pas
var
  rsl : String;
//  S: RawByteString;
  ww : RawByteString;
begin
//  S := DupChar(131, $aa);

  ww := SHA256DigestToHexA(flcHash.CalcHMAC_SHA256(UTF8Encode(Edit2.Text),
          UTF8Encode(Edit1.Text)));
  Setstring(rsl, PAnsiChar(ww), Length(ww));
  Memo1.Lines.Add('');
  Memo1.Lines.Add('--------flcHash.pas--------');
  Memo1.Lines.Add('flcHash HMAC_SHA256:' +#13#10 + rsl);
  Memo1.Lines.Add('');
  ww := SHA256DigestToHexA(flcHash.CalcSHA256(UTF8Encode(Edit1.Text)));
  Setstring(rsl, PAnsiChar(ww), Length(ww));
  Memo1.Lines.Add('flcHash SHA256:' +#13#10 + rsl);

  // fundamentals5 File hash

end;

procedure TForm1.Button8Click(Sender: TObject);
begin
  Memo1.Clear;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  inherited;
  // UAC权限 使用下面这三行
  ChangeWindowMessageFilter(WM_DROPFILES, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_COPYDATA, MSGFLT_ADD);
  ChangeWindowMessageFilter(WM_COPYGLOBALDATA , MSGFLT_ADD);
  DragAcceptFiles(Self.Handle, True);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  DragAcceptFiles(Self.Handle, False);
  inherited;
end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    Edit3.Text := OpenDialog1.FileName;
  end;
end;

procedure TForm1.TFfile(var Msg: TMessage);
var
  buffer:array[0..1024] of Char;
begin
  inherited;
  try
    buffer[0]:=#0;
    DragQueryFile(Msg.WParam,0,buffer,sizeof(buffer)); //获取拖入文件名称
    Edit3.Text:=PChar(@buffer); //字符数组转换成字符串
  finally
    DragFinish(Msg.WParam);
  end;
end;


end.
