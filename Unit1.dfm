object Form1: TForm1
  Left = 617
  Top = 231
  Caption = 'Hash Tool'
  ClientHeight = 407
  ClientWidth = 749
  Color = clBtnFace
  Constraints.MinHeight = 360
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    749
    407)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 48
    Top = 25
    Width = 72
    Height = 13
    Caption = #24453#21152#23494#23383#31526#20018
  end
  object Label2: TLabel
    Left = 102
    Top = 65
    Width = 18
    Height = 13
    Caption = 'Key'
  end
  object Label3: TLabel
    Left = 60
    Top = 103
    Width = 60
    Height = 13
    Caption = #24453#21152#23494#25991#20214
  end
  object SpeedButton1: TSpeedButton
    Left = 562
    Top = 99
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = SpeedButton1Click
  end
  object Button1: TButton
    Left = 60
    Top = 137
    Width = 109
    Height = 41
    Caption = 'Delphi System.Hash'
    TabOrder = 5
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 126
    Top = 22
    Width = 300
    Height = 21
    TabOrder = 2
    Text = 'Edit1'
  end
  object Edit2: TEdit
    Left = 126
    Top = 62
    Width = 300
    Height = 21
    TabOrder = 3
    Text = 'Edit2'
  end
  object Memo1: TMemo
    Left = 41
    Top = 195
    Width = 667
    Height = 173
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssBoth
    TabOrder = 0
    ExplicitHeight = 143
  end
  object Button2: TButton
    Left = 182
    Top = 137
    Width = 109
    Height = 41
    Caption = 'ChilKat Crypt2'
    TabOrder = 6
    OnClick = Button2Click
  end
  object Edit3: TEdit
    Left = 126
    Top = 100
    Width = 436
    Height = 21
    ReadOnly = True
    TabOrder = 4
  end
  object Button3: TButton
    Left = 511
    Top = 22
    Width = 138
    Height = 41
    Caption = 'TC3-HMAC-SHA256'
    TabOrder = 1
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 304
    Top = 137
    Width = 109
    Height = 41
    Caption = 'DEC HMAC_SHA256'
    TabOrder = 7
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 270
    Top = 374
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'Json'
    TabOrder = 10
    OnClick = Button5Click
    ExplicitTop = 344
  end
  object Button6: TButton
    Left = 426
    Top = 137
    Width = 109
    Height = 41
    Caption = 'HashLib4Pascal'
    TabOrder = 8
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 548
    Top = 137
    Width = 109
    Height = 41
    Hint = 'fundamentals5'
    Caption = 'flcCryptoHash'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 431
    Top = 374
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'ClearMemo'
    TabOrder = 11
    OnClick = Button8Click
    ExplicitTop = 344
  end
  object OpenDialog1: TOpenDialog
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 168
    Top = 248
  end
end
